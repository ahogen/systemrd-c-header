#!/usr/bin/env python3
import os
import mmap
import pathlib
import io
import logging
import textwrap
from datetime import datetime
from typing import Union, Optional, List

# third-party
import systemrdl
import pathvalidate


class HeaderExporter:
    def __init__(self, **kwargs):
        """Create a C header file from a SystemRDL description.

        My idea/thought process so far -- Will try to map SystemRDL to a C
        header like this:

        | SystemRDL   | C Header    |
        |-------------|-------------|
        | addrmap     | Header file |
        | regfile     | struct      |
        | reg         | #define <address> |
        | field       | #define <mask> / <position> |
        | signal      | [!] Unsupported |
        | memory      | [!] Unsupported (but shouldn't be hard to add as a struct?) |
        | counter     | [!] Unsupported |
        | interrupt   | [!] Unsupported |

        """

        self._log = logging.getLogger(__name__)

        # Check for stray kwargs
        if kwargs:
            raise TypeError(
                "got an unexpected keyword argument '%s'" % list(kwargs.keys())[0]
            )

        self.line_len = 75
        self.doc_line_prefix = " * "
        # List of enum types that have been defined
        self.generated_enums = []
        # List of structure types that have been defined
        self.generated_structs = []
        # The define name of the base address of a register bank (AddressMap)
        self.base_address_name: str

        # Dictionary of root-level type definitions
        # key = definition type name
        # value = representative object
        #   components, this is the original_def (which can be None in some cases)
        self._defined_types = {}

        self._instantiation_defines: List[str] = []

        # Top-level addrmap
        self.top = None
        self._fout = None
        self._is_lsb0 = True
        self._addrmap_count = 0

    # ---------------------------------------------------------------------------
    def export(
        self,
        rdlc: systemrdl.RDLCompiler,
        obj: Union[systemrdl.node.AddrmapNode, systemrdl.node.RegfileNode],
        path: pathlib.Path,
    ):
        path = path.absolute().resolve()
        output_dir = path.parent

        if path.is_dir():
            output_dir = path
            path = path / obj.top.name.replace(" ", "_") + ".h"

        # Make sure output directory structure exists
        output_dir.mkdir(parents=True, exist_ok=True)

        self._addrmap_count = 0

        # If it is the root node, skip to top addrmap
        if isinstance(obj, systemrdl.node.RootNode):
            obj = obj.top
        self.top = obj

        # Add header comment
        head_comment = "@file \n"
        head_comment += (
            "@date " + datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ") + "\n"
        )
        head_comment += "@note GENERATED FILE\n"
        # head_comment += "From: <tool name> <tool version>" +
        head_comment += "\n"
        head_comment += "I don't yet have these things working in the C exporter:\n\n"
        head_comment += (
            "@todo Multiple addrmap nodes (ideally split into seperate header files)\n"
        )
        head_comment += "@todo arrays of registers / regfiles (almost there)\n"
        head_comment += "@todo simple defines for register field masks\n"
        head_comment += "@todo endianness handling (?)\n"
        head_comment = self._create_docblock(head_comment)

        with open(str(path), "w", encoding="utf-8", buffering=mmap.PAGESIZE) as fout:
            self._fout = fout

            fout.write(head_comment + "\n")

            # Add top of include guard now.
            include_guard_def = path.name.upper().replace(".", "_")
            include_guard_def = include_guard_def.replace("-", "_")
            fout.write("#ifndef {0}\n#define {0}\n\n".format(include_guard_def))

            # Add standard include for fixed-width types
            fout.write("#include <stdint.h>\n")
            fout.write("#include <stddef.h>\n\n")

            fout.write("#ifdef  __cplusplus\n" 'extern "C"\n' "{\n" "#endif\n\n")

            fout.write(
                "#ifndef PEAKRDL_C_HEADER_HELPER_DEFS\n"
                "#define PEAKRDL_C_HEADER_HELPER_DEFS\n"
                "/*! Get the address of a register that was defined inside a struct. \n"
                " * Used to get a single register's address out of a RegFile structure pointer. \n"
                " * \n"
                " *     struct UART_Type {\n"
                " *       uint8_t rx_data; \n"
                " *       uint8_t tx_data; \n"
                " *       uint8_t baud_div;\n"
                " *       uint8_t ctrl;\n"
                " *     };\n"
                " *     #define UART0 (struct UART_Type*)<UART_BASE_ADDR>\n"
                " * \n"
                " * Then use REG_ADDR(UART0, baud_div) to the get address of the BAUD_DIV register.\n"
                " */\n"
                "#define REG_ADDR(_regfile_ptr, _reg_member) ((uintptr_t)(&(_regfile_ptr->_reg_member)))\n"
                "#endif /* PEAKRDL_C_HEADER_HELPER_DEFS */\n\n"
            )

            self.base_address_name = obj.inst_name.upper() + "_BASE_ADDR"
            self._fout.write(
                "/*! Base address of the register map */\n"
                "#define {} 0x{:x}\n\n".format(
                    self.base_address_name, obj.absolute_address
                )
            )

            # Start converting the address map
            self._convert_addrmap_or_regfile(rdlc, obj)

            fout.write("\n")
            for def_str in self._instantiation_defines:
                fout.write(def_str)
            fout.write("\n")

            fout.write("\n#ifdef  __cplusplus\n" "}\n" "#endif\n")

            # "endif" include block
            fout.write("\n#endif /* {:s} */\n".format(include_guard_def))

        self._fout = None
        del self._fout

    def addressmap_output_filename(self, node: systemrdl.node.AddrmapNode) -> str:
        """Generate a C header filename string, given an Address Map node.

        Returns:
            Filename string, e.g. "my_reg
        """

        if not isinstance(node, systemrdl.node.AddrmapNode):
            raise TypeError("Expected an Address Map")

        fname = node.inst_name
        if not fname:
            fname = node.get_scope_path(hier_separator="_")

        return pathvalidate.sanitize_filename(fname + ".h")

    def _create_docblock(self, txt: str) -> str:
        "Wrap text and prepend asterisks to create a Doxygen-like doc block"

        lines = txt.splitlines()
        reflowed = ""
        for i in range(len(lines)):
            reflowed += (
                "\n"
                + self.doc_line_prefix
                + textwrap.fill(lines[i], subsequent_indent=self.doc_line_prefix)
            )
        return "\n/** {}\n */".format(reflowed)

    def _convert_addrmap_or_regfile(
        self,
        rdlc: systemrdl.RDLCompiler,
        obj: Union[systemrdl.node.AddrmapNode, systemrdl.node.RegfileNode],
    ):
        if isinstance(obj, systemrdl.node.AddrmapNode):
            self._addrmap_count += 1

            if self._addrmap_count > 1:
                rdlc.msg.error(
                    "Exporter does not yet handle nested Address Maps.",
                    obj.inst.inst_src_ref,
                )
                self._log.warning("Issued RDL error message about unsupported type")
        elif isinstance(obj, systemrdl.node.RegfileNode):
            self._convert_regfile(rdlc, obj)
        elif isinstance(obj, systemrdl.node.RegNode):
            self._convert_reg(rdlc, obj)
        else:
            raise RuntimeError

        for child in obj.children():
            # The Regnode children of a RegfileNode are exported by the
            # _convert_regfile() method (above). Ignore Regnode here.
            if isinstance(obj, systemrdl.node.RegfileNode) and isinstance(
                child, systemrdl.node.RegNode
            ):
                continue
            elif isinstance(
                child, (systemrdl.node.AddrmapNode, systemrdl.node.RegfileNode)
            ):
                self._convert_addrmap_or_regfile(rdlc, child)
            elif isinstance(child, systemrdl.node.RegNode):
                self._convert_reg(rdlc, child)
            else:
                rdlc.msg.error(
                    'Exporter does not yet handle node type "{}"'.format(type(child)),
                    child.inst.inst_src_ref,
                )
                self._log.warning("Issued RDL error message about unsupported type")
                # return

    def _convert_regfile(self, rdlc, obj: systemrdl.node.RegfileNode):
        """Export a regfile object as a C struct.


        Note that the SystemRDL spec says that in a RegFile node:

        > ...the only components that can be instantiated shall be a regfile,
        > reg, constraint, and signal.
        """
        for child in obj.children():
            if isinstance(child, systemrdl.node.RegfileNode):
                self._convert_regfile(rdlc, child)
            elif isinstance(child, systemrdl.node.RegNode):
                # Has registers, create C structure
                self._regfile_to_c_struct(rdlc, obj)

            # elif isinstance(child, systemrdl.node.SignalNode):
            else:
                rdlc.msg.error(
                    f'Exporter does not yet handle node type "{type(child)}" in Regfile',
                    child.inst.inst_src_ref,
                )
                self._log.warning("Issued RDL error message about unsupported type")
                return

        # Create an instance of the struct type at absolute address
        if obj.is_array:
            if len(obj.array_dimensions) > 1:
                rdlc.msg.error(
                    "Exporter does not yet support multi-dimensional arrays",
                    obj.inst.inst_src_ref,
                )
                self._log.warning("Issued RDL error about unsupported type")
                return

            for i, ra_inst in enumerate(obj.unrolled()):
                inst_name = ra_inst.inst_name
                for x in ra_inst.current_idx:
                    inst_name += "_{:d}".format(x)

                self._instantiation_defines.append(
                    "#define {} ((volatile struct {}_{} *)0x{:X}U)\n".format(
                        inst_name.upper(),
                        ra_inst.parent.inst_name,
                        ra_inst.type_name,
                        ra_inst.absolute_address,
                    )
                )
        else:
            self._instantiation_defines.append(
                f"#define {obj.inst_name.upper():s} "
                f"((volatile struct {obj.inst.type_name} *const)0x{obj.absolute_address:X}U)\n"
            )

    def _regfile_to_c_struct(self, rdlc, obj: systemrdl.node.RegfileNode):
        struct_regs = ""
        # Minimum access width of all registers in this regfile
        min_access_width = None
        # List of register field defines to create after the regfile struct
        reg_field_defines: List[str] = []

        internal_type_name = self._get_type_name(obj)
        if internal_type_name in self._defined_types:
            return

        # First, create a C structure of the children
        for child in obj.children():
            if not isinstance(child, systemrdl.RegNode):
                # Types of non-register nodes should have been taken care of
                # earlier, in _convert_regfile().
                # Just need to instantiate it here.
                struct_regs += "struct {} {};\n".format(
                    child.type_name, child.inst_name
                )
                continue

            c_type = "UNSUPPORTED_STD_C_TYPE"
            reg_width = child.get_property("regwidth")
            # SystemRDL 2.0 section 6.2.1 says all types are unsigned
            if reg_width == 8:
                c_type = "uint8_t"
            elif reg_width == 16:
                c_type = "uint16_t"
            elif reg_width == 32:
                c_type = "uint32_t"
            elif reg_width == 64:
                c_type = "uint64_t"
            else:
                rdlc.msg.warning(
                    "Unsure what C type to use for this register",
                    child.inst.inst_src_ref,
                )

            if child.has_sw_readable and not child.has_sw_writable:
                c_type = "const " + c_type

            struct_regs += c_type + " " + child.inst_name + ";\n"

            if min_access_width:
                min_access_width = min(
                    min_access_width, child.get_property("accesswidth")
                )
            else:
                min_access_width = child.get_property("accesswidth")

            reg_rst_val = 0
            for field in child.fields():
                reg_rst_val |= field.get_property("reset", default=0) << field.low

            reg_field_defines.append(
                "#define {}_{}_RstVal 0x{:0{width}X}U\n".format(
                    obj.inst_name.upper(),
                    child.inst_name.upper(),
                    reg_rst_val,
                    width=int(child.get_property("regwidth") / 4),
                )
            )

            for field in child.fields():
                self._convert_field(rdlc, field, reg_field_defines)

            reg_field_defines.append("\n")

        align = int(min_access_width / 8)

        self._write(
            f"\nstruct __attribute__((__packed__, __aligned__({align:d}))) "
            + f"{obj.type_name}"
            + "{"
            + struct_regs
            + "};\n\n"
        )

        # Write the regiser field pos/msk defines to the file.
        self._write("".join(reg_field_defines) + "\n")

        # Mark this structure's type as being defined now, so it won't get
        # generated again.
        self._defined_types[internal_type_name] = obj

    def _convert_reg(self, rdlc, obj: systemrdl.node.RegNode):
        if obj.is_array:
            rdlc.msg.error(
                "Exporter does not yet support arrays", obj.inst.inst_src_ref
            )
            self._log.warning("Issued RDL error message about unsupported type")
            return

        # Convert information about the register
        reg_doc_comment = obj.get_property("name", default=None)
        if not reg_doc_comment:
            reg_doc_comment = obj.parent.inst_name + "."
        else:
            reg_doc_comment = reg_doc_comment.replace("\n", "")
        reg_doc_comment += "\n\n"

        desc = obj.get_property("desc")
        if desc:
            reg_doc_comment += desc.replace("\n", " ").replace("\r", "")
            reg_doc_comment += "\n\n"

        for field in obj.fields():
            if field.msb == field.lsb:
                reg_doc_comment += "- [{:d}]".format(field.msb)
            else:
                reg_doc_comment += "- [{:d}:{:d}]".format(field.msb, field.lsb)
            reg_doc_comment += " " + field.inst_name

            desc = field.get_property("desc")
            if desc:
                reg_doc_comment += ": " + desc.replace("\n", " ").strip()
            reg_doc_comment += "\n"
        reg_doc_comment += "\n[SystemRDL path: '" + obj.get_path() + "']"

        self._write(self._create_docblock(reg_doc_comment) + "\n")
        self._write(
            "#define {:s}_{:s} ((uintptr_t)({:s} + {:#x}))\n".format(
                obj.parent.inst_name.upper(),
                obj.inst_name.upper(),
                self.base_address_name,
                obj.address_offset,
            )
        )

        # Iterate over all the fields in this reg and convert them
        reg_rst_val = 0
        for field in obj.fields():
            r = field.get_property("reset", default=0)
            if isinstance(r, systemrdl.node.FieldNode):
                # If an instance reference was returned, get the reset value of
                # that instance.
                r = r.get_property("reset", default=0)

            reg_rst_val |= r << field.low

        self._write("/** Reset value of `{}` */\n".format(obj.inst_name))
        self._write(
            "#define {:s}_{:s}_RstVal 0x{:0{width}x}U\n".format(
                obj.parent.inst_name.upper(),
                obj.inst_name.upper(),
                reg_rst_val,
                width=int(obj.get_property("regwidth") / 4),
            )
        )

        for field in obj.fields():
            self._convert_field(rdlc, field)

    def _convert_field(
        self, rdlc, obj: systemrdl.node.FieldNode, strlist: Optional[List[str]] = None
    ):
        field_name = "{:s}_{:s}_{:s}".format(
            obj.parent.parent.inst_name.upper(),
            obj.parent.inst_name.upper(),
            obj.inst_name.upper(),
        )

        s = "#define {:s}_Pos {:d}U\n".format(field_name, obj.low)
        if strlist:
            strlist.append(s)
        else:
            self._write(s)

        mask_value = (1 << obj.width) - 1
        s = "#define {:s}_Msk (0x{:X}U << {})\n".format(field_name, mask_value, obj.low)
        if strlist:
            strlist.append(s)
        else:
            self._write(s)

        # TODO: Add cast for enum types?
        # maybe_const_str = ""
        # if not obj.is_sw_writable:
        #     maybe_const_str = "const"
        # self._write(
        #     "#define {:s}_Val(reg_value) ({:s}( (reg_value >>  {}) & {:#X}U))\n".format(
        #         field_name, maybe_const_str, obj.low, mask_value
        #     )
        # )

    def _write(self, string: str):
        self._fout.write(string)

    def _get_type_name(self, node: systemrdl.node.Node):
        class_name = self._get_resolved_scope_path(node, "__")

        if not class_name:
            # Unable to determine a reusable type name. Fall back to hierarchical path
            class_name = node.get_rel_path(
                self.top.parent,
                hier_separator="__",
                array_suffix="",
                empty_array_suffix="",
            )

        return class_name

    def _get_resolved_scope_path(
        self, node: systemrdl.node.Node, separator: str = "::"
    ) -> Optional[str]:
        """
        Returns the scope path, but with resolved type names
        Returns None if any segment in the path is unknown

        Borrowed from PeakRDL-uvm, src/peakrdl_uvm/exporter.py
        https://github.com/SystemRDL/PeakRDL-uvm/blob/aec9ad4441bea3f51e5c08163a92eccad4051cfc/src/peakrdl_uvm/exporter.py#L190
        """

        if node.inst.parent_scope is None:
            # Scope information is not known
            return None

        if isinstance(node.inst.parent_scope, systemrdl.component.Root):
            # Declaration of this was in the root scope
            return node.type_name

        # Due to namespace nesting properties, it is guaranteed that the parent
        # scope definition is also going to be one of the node's ancestors.
        # Seek up and find it
        current_parent_node = node.parent

        while current_parent_node:
            if current_parent_node.inst.original_def is None:
                # Original def reference is unknown
                return None
            if current_parent_node.inst.original_def is node.inst.parent_scope:
                # Parent node's definition matches the scope we're looking for
                parent_scope_path = self._get_resolved_scope_path(
                    current_parent_node, separator
                )
                if (parent_scope_path is None) or (node.type_name is None):
                    return None
                return parent_scope_path + separator + node.type_name

            current_parent_node = current_parent_node.parent

        # Failed to find the path
        return None

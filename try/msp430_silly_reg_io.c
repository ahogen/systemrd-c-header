#include <stdio.h>
#include <stdlib.h>

#include "msp430g2553.h"

void __stack_chk_fail(void);

int main(int argc, char* argv[]) {
  (void)argc;
  (void)argv;

  printf("Creating a timer\n");
  uint8_t timer_0_memory[sizeof(struct Timer_A3_Type)] = {0};
  struct Timer_A3_Type* timer_A0 = (struct Timer_A3_Type*)&timer_0_memory;

  timer_A0->ccr0 = 1;
  // timer_A0->ctl.clr = 1;

  printf("Addr of TAR reg in timer_a0: 0x%I64X\n", REG_ADDR(timer_A0, tar));

  printf("Addr of CCR0 reg in TIMER0_A3: 0x%I64X\n", REG_ADDR(TIMER0_A3, ccr0));

  printf("Bye\n");

  return 0;
}

/*
 * Stack canary / guard and other useful GCC checkers will call functions here.
 * For example, if "-fstack-protector" is defined in compile flags, the
 * "__stack_chk_fail()" function here will get called if a failure is detected.
 *
 * Refer to GCC instrumentation build flags documentation:
 * https://gcc.gnu.org/onlinedocs/gcc/Instrumentation-Options.html#Instrumentation-Options
 */
void __stack_chk_fail(void) {
  puts("\n\n" __FILE__ ": ERROR: Stack check failed!\n");
}
unsigned long __stack_chk_guard = 0xDEADA5A5;

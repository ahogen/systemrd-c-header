#!/usr/bin/env python3

import sys
import os
import subprocess
import logging
import pathlib

# third-party
import systemrdl
from systemrdl import RDLCompiler, RDLCompileError
import coloredlogs

# Ignore this. Only needed for this example
this_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.join(this_dir, "../"))

from peakrdl_c_header import HeaderExporter

# TODO: Remove HTML stuff. Useful atm to compare with what Alex Mykyta already has
import peakrdl_html

log = logging.getLogger(os.path.splitext(os.path.basename(__file__))[0])

C_CPP_FORMAT_TOOL_BIN = "clang-format"
C_CPP_FORMAT_TOOL_ARGS = ["-i", "-style=WebKit"]


def main():
    log.info(main.__name__)

    # Collect input files from the command line arguments
    input_file = sys.argv[1]

    # Create an instance of the compiler
    rdlc = RDLCompiler()

    fname_out = pathlib.Path(os.path.dirname(__file__) + "/..")
    fname_out = fname_out / "out" / pathlib.Path(input_file).name
    fname_out = fname_out.absolute().resolve()

    try:
        file_info: systemrdl.compiler.FileInfo = rdlc.preprocess_file(input_file)
        log.info("{} includes in {}".format(len(file_info.included_files), input_file))

        input_file_preprocessed = fname_out.with_suffix(".rdl.pp")
        os.makedirs(str(input_file_preprocessed.parent), exist_ok=True)

        with open(input_file_preprocessed, mode="w", encoding="utf-8") as pp_fout:
            pp_fout.write(file_info.preprocessed_text)

        # Compile the file provided
        log.info("Compiling")
        rdlc.compile_file(input_file_preprocessed)

        # Elaborate the design
        log.info("Elaborating")
        root = rdlc.elaborate()
    except RDLCompileError:
        # A compilation error occurred. Exit with error code
        sys.exit(1)

    # html_exporter = peakrdl_html.HTMLExporter()
    # log.info("Generating HTML: %s" % fname_out)
    # html_exporter.export(nodes=root, output_dir=str(fname_out.with_suffix("")))

    log.info("Generating C Header: %s" % fname_out)
    headerfile = HeaderExporter()
    headerfile.export(rdlc, obj=root, path=fname_out.with_suffix(".h"))

    formatter_cmd = [C_CPP_FORMAT_TOOL_BIN]
    formatter_cmd.extend(C_CPP_FORMAT_TOOL_ARGS)
    formatter_cmd.append(fname_out.with_suffix(".h"))
    log.info("Running C formatter ({})".format(formatter_cmd))
    subprocess.run(args=formatter_cmd)


if __name__ == "__main__":
    lvl_style = coloredlogs.DEFAULT_LEVEL_STYLES
    # lvl_style["warning"] = {"color": "yellow", "bold": True}
    coloredlogs.install(
        level="DEBUG",
        fmt="%(msecs)d %(filename)s:%(lineno)d [%(levelname)s] %(message)s",
        level_styles=lvl_style,
    )

    main()
